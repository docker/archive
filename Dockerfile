FROM igwn/base:el9

ARG IGWN_ARCHIVE_VERSION="0.1.1"

LABEL maintainer="Duncan Macleod <duncan.macleod@ligo.org>"
LABEL version="${IGWN_ARCHIVE_VERSION}"
LABEL description="IGWN Archive upload utility container"
LABEL support="Best Effort"

# install available updates
RUN dnf -y update \
    && dnf clean all

# install igwn-archive
RUN dnf -y install igwn-archive-${IGWN_ARCHIVE_VERSION} \
    && dnf clean all
